import { Component, OnInit } from '@angular/core';
import { Response, RequestOptions,
         Headers } from '@angular/http';

import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from 'app/security/authentication.service';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent implements OnInit {


  ngOnInit(): void {
  }

  constructor(private authenticationService: AuthenticationService) {
  }

  hasRole(role: string): boolean {
    return this.authenticationService.hasRole(role);
  }
}
