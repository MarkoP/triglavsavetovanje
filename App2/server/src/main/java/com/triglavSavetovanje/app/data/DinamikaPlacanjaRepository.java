package com.triglavSavetovanje.app.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.triglavSavetovanje.app.model.DinamikaPlacanja;

@Repository
public interface DinamikaPlacanjaRepository extends JpaRepository<DinamikaPlacanja, Long> {

}
