package com.triglavSavetovanje.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.triglavSavetovanje.app.data.OsiguranikRepository;
import com.triglavSavetovanje.app.model.Osiguranik;

@Component
public class OsiguranikService {
	
	@Autowired
	OsiguranikRepository osiguranikRepository;

	public <S extends Osiguranik> S save(S entity) {
		return osiguranikRepository.save(entity);
	}

	public Page<Osiguranik> findAll(Pageable pageable) {
		return osiguranikRepository.findAll(pageable);
	}

	public List<Osiguranik> findAll() {
		return osiguranikRepository.findAll();
	}

	public Osiguranik findOne(Long id) {
		return osiguranikRepository.findOne(id);
	}

	public void delete(Long id) {
		osiguranikRepository.delete(id);
	}
	
	
}
