package com.triglavSavetovanje.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Preporuka {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String ime;
	private String prezime;
	private String mesto;
	private String brojTelefona;
	@Column(length = 5000)
	private String opis;

	public Preporuka() {
		super();
	}
	public String getBrojTelefona() {
		return brojTelefona;
	}
	public Long getId() {
		return id;
	}
	public String getIme() {
		return ime;
	}
	public String getMesto() {
		return mesto;
	}
	public String getOpis() {
		return opis;
	}

	public String getPrezime() {
		return prezime;
	}
	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public void setMesto(String mesto) {
		this.mesto = mesto;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	
	
	
}
