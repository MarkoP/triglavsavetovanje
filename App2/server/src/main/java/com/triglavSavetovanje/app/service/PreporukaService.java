package com.triglavSavetovanje.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.triglavSavetovanje.app.data.PreporukaRepository;
import com.triglavSavetovanje.app.model.Preporuka;

@Component
public class PreporukaService {
	
	@Autowired
	PreporukaRepository preporukaRepository;

	public <S extends Preporuka> S save(S entity) {
		return preporukaRepository.save(entity);
	}

	public Page<Preporuka> findAll(Pageable pageable) {
		return preporukaRepository.findAll(pageable);
	}

	public List<Preporuka> findAll() {
		return preporukaRepository.findAll();
	}

	public Preporuka findOne(Long id) {
		return preporukaRepository.findOne(id);
	}

	public void delete(Long id) {
		preporukaRepository.delete(id);
	}
	
	
}
