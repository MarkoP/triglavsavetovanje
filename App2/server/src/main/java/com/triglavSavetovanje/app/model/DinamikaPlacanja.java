package com.triglavSavetovanje.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class DinamikaPlacanja {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String dinamika;

	public DinamikaPlacanja() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDinamika() {
		return dinamika;
	}

	public void setDinamika(String dinamika) {
		this.dinamika = dinamika;
	}
	
	
}
