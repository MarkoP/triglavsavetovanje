package com.triglavSavetovanje.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class NacinPlacanja {
	
	@Id
	@GeneratedValue
	private Long id;
	private String nacin;
	
	public NacinPlacanja() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNacin() {
		return nacin;
	}

	public void setNacin(String nacin) {
		this.nacin = nacin;
	}
	
	
}
