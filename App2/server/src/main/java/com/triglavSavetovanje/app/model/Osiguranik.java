package com.triglavSavetovanje.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class Osiguranik {
	@Id
	@GeneratedValue
	private Long id;

	private String ime;
	private String prezime;
	private String jmbg;
	private String mesto;
	private String ulicaBroj;
	private String telefon;
	private String pol;
	private String vrstaPolise;
	private int trajanjePolise;
	private int visinaPremije;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private NacinPlacanja nacinPlacanja;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private DinamikaPlacanja dinamikaPlacanja;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Preporuka>preporuke = new HashSet<Preporuka>();
	

	public Osiguranik() {
		super();
	}


	public DinamikaPlacanja getDinamikaPlacanja() {
		return dinamikaPlacanja;
	}


	public Long getId() {
		return id;
	}


	public String getIme() {
		return ime;
	}


	public String getJmbg() {
		return jmbg;
	}


	public String getMesto() {
		return mesto;
	}


	public NacinPlacanja getNacinPlacanja() {
		return nacinPlacanja;
	}


	public String getPol() {
		return pol;
	}


	public Set<Preporuka> getPreporuke() {
		return preporuke;
	}


	public String getPrezime() {
		return prezime;
	}


	public String getTelefon() {
		return telefon;
	}


	public int getTrajanjePolise() {
		return trajanjePolise;
	}


	public String getUlicaBroj() {
		return ulicaBroj;
	}


	public int getVisinaPremije() {
		return visinaPremije;
	}


	public String getVrstaPolise() {
		return vrstaPolise;
	}


	public void setDinamikaPlacanja(DinamikaPlacanja dinamikaPlacanja) {
		this.dinamikaPlacanja = dinamikaPlacanja;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}


	public void setMesto(String mesto) {
		this.mesto = mesto;
	}


	public void setNacinPlacanja(NacinPlacanja nacinPlacanja) {
		this.nacinPlacanja = nacinPlacanja;
	}


	public void setPol(String pol) {
		this.pol = pol;
	}


	public void setPreporuke(Set<Preporuka> preporuke) {
		this.preporuke = preporuke;
	}


	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}


	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}


	public void setTrajanjePolise(int trajanjePolise) {
		this.trajanjePolise = trajanjePolise;
	}


	public void setUlicaBroj(String ulicaBroj) {
		this.ulicaBroj = ulicaBroj;
	}


	public void setVisinaPremije(int visinaPremije) {
		this.visinaPremije = visinaPremije;
	}


	public void setVrstaPolise(String vrstaPolise) {
		this.vrstaPolise = vrstaPolise;
	}
	
	
}
