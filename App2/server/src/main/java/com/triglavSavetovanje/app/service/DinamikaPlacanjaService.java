package com.triglavSavetovanje.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.triglavSavetovanje.app.data.DinamikaPlacanjaRepository;
import com.triglavSavetovanje.app.model.DinamikaPlacanja;

@Component
public class DinamikaPlacanjaService {
	
	@Autowired
	DinamikaPlacanjaRepository dinamikaPlacanjaRepository;

	public <S extends DinamikaPlacanja> S save(S entity) {
		return dinamikaPlacanjaRepository.save(entity);
	}

	public List<DinamikaPlacanja> findAll() {
		return dinamikaPlacanjaRepository.findAll();
	}

	public DinamikaPlacanja findOne(Long id) {
		return dinamikaPlacanjaRepository.findOne(id);
	}

	public void delete(Long id) {
		dinamikaPlacanjaRepository.delete(id);
	}
	
	
}
