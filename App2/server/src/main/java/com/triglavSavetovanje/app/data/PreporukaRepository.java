package com.triglavSavetovanje.app.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.triglavSavetovanje.app.model.Preporuka;

@Repository
public interface PreporukaRepository extends JpaRepository<Preporuka, Long> {

}
