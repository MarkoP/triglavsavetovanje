package com.triglavSavetovanje.app.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.triglavSavetovanje.app.model.Osiguranik;

@Repository
public interface OsiguranikRepository extends JpaRepository<Osiguranik, Long> {

}
