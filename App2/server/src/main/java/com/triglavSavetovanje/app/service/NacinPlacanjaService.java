package com.triglavSavetovanje.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.triglavSavetovanje.app.data.NacinPlacanjaRepository;
import com.triglavSavetovanje.app.model.NacinPlacanja;

@Component
public class NacinPlacanjaService {

	@Autowired
	NacinPlacanjaRepository nacinPlacanjaRepository;

	public <S extends NacinPlacanja> S save(S entity) {
		return nacinPlacanjaRepository.save(entity);
	}

	public List<NacinPlacanja> findAll() {
		return nacinPlacanjaRepository.findAll();
	}

	public NacinPlacanja findOne(Long id) {
		return nacinPlacanjaRepository.findOne(id);
	}

	public void delete(Long id) {
		nacinPlacanjaRepository.delete(id);
	}

	
	
}
